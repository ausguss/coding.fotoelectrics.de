+++
date = "2015-10-18T22:03:06+02:00"
draft = false
title = "Author"
description = "Josa Wode is a programmer and software developer (author of code). What more?"
+++
<img class="autoscaled leftfloat" alt="Author Josa Wode on his daily research (Photo: Tom Meng)" src="../img/josa-wode.png" style="margin-top:12px;" />
**For information on me as a writer of fiction please visit [writing.fotoelectrics.de/author](http://writing.fotoelectrics.de/author).**

Josa Wode is a programmer and software developer -- that is author of code -- who after graduation in computer science at the Philipps-Universität Marburg worked for about one and a half year as a developer in a small software company in Dresden before deciding to make a change. 
He certainly learned a lot -- not only about developing software.
For the first half of 2016 he worked enthusiastically on an IoT project for [Green City Solutions](http://www.greencitysolutions.de). Now he does some only vaguely sustainable stuff in part time for a big company to earn his living. In his own time he works at other stuff like the projects on this site or his stories.

I am curious what he is going to do next.  
He surely would love to work on solutions with a positive social or environmental impact.

