+++
date = "2015-10-26T18:31:00+01:00"
description = "Source repositories for my websites created with Hugo Static Website Generator"
title = "Website with Hugo"

+++
The website you are currently on and my [writers website](http://writing.fotoelectrics.de) were created with [Hugo](http://gohugo.io), a static website generator. 
You may use the source code for your own purposes or just have a curious look.

#### Source  
writing.fotoelectrics.de &ndash; [Bitbucket Repository](https://bitbucket.org/ausguss/writing.fotoelectrics.de/)  
coding.fotoelectrics.de &ndash; [Bitbucket Repository](https://bitbucket.org/ausguss/coding.fotoelectrics.de)
