+++
date = "2018-05-24"
description = "GUI tool for markup to LaTeX and Html conversion"
title = "Textstory Creator with Python and Kivy"

+++
A while ago -- when I wrote [this piece](http://writing.fotoelectrics.de/texts/come-get-her) (a short story in German) and was too lazy to do proper layouting -- a [friend](http://www.gehrcke.de) made me a beautiful present:



[textstory-to-beautiful-latex-html](https://bitbucket.org/jgehrcke/textstory-to-beautiful-latex-html/src/default/) &ndash; he coded a conversion tool for text documents written in a simple markup language (not far from [Markdown](https://daringfireball.net/projects/markdown/syntax)) that spits out perfectly handsome Html and LaTeX (the latter of course for conversion to pdf) and voilà: my story looked nice.



Then after some minor fixes the project just lay there and was only sporadically used by myself &ndash; until all of a sudden I went crazy and started adding a lot of features like new markup commands and a setup file to make the whole thing configurable (it's all in the documentation).



I still have a lot of ideas about new features (that I will hopefully get around implementing at some point), but I thought that it would be cool to first make the whole thing easier to use by people not feeling at home on the command line. The idea *Textstory Creator* was born and here it is:



[Textstory Creator](https://bitbucket.org/ausguss/textstory_creator/)



To make things as easy as possible I put some effort into creating at least an installer for Windows and Debian based systems, but manual installation is also well documented and quite easy thanks to *Python* and *pip*. 



By the way, documentation is provided in German and English. Feel free to translate to your favourite language and I will be happy to add this -- preferably when the translation is already styled in [Markdown](https://daringfireball.net/projects/markdown/syntax) (you may have a look at the code of README.md to get the idea). The same goes for the *Textstory Creator* itself. You could install [Poedit](https://poedit.net/) or use some other tool to import the *pot* file from the projects *locale* directory and create a translation *po* and *mo* file.



That's about it. Everything else you can get from [documentation](https://bitbucket.org/ausguss/textstory_creator/src/master/README.md), [source code](https://bitbucket.org/ausguss/textstory_creator/src) or by <a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">mailing me</a> (do not hesitate).

[Here](http://writing.fotoelectrics.de/texts/tutorial-textstorycreator/) you find a detailed tutorial in German.

*Future plan: Writing a [WYSIWYM](https://en.wikipedia.org/wiki/WYSIWYM) editor for textstory files. I hope that is not too ambitious.*
