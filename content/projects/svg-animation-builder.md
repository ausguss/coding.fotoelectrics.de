+++
date = "2016-03-06T00:36:41+01:00"
description = "A small python tool that takes (png or svg) images from a folder to generate an animated svg."
title = "Create Stop Motion Animated SVGs in Python"

+++
For my brothers website (still under construction) animated gifs showed their limits &mdash; no alpha-channel for transparency (and limited colors).
After some research and experimentation animated svg images seemed to be a good option (CSS3 animation proved slow and limited at the moment &mdash; although a promising development &mdash; and it is always a welcome challenge to avoid JavaScript).
SVG provides many features and is supported by modern web browsers.

I coded this little helper to make my life easier by taking a bunch of images from a given folder and spitting out svg stop-motion animation.
More information can be found in the readme file coming with the source code.

#### Source
svg-animation-builder &mdash; [Bitbucket Repository](https://bitbucket.org/ausguss/svg-animation-builder/)
