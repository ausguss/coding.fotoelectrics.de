+++
date = "2015-05-15T14:54:49+02:00"
draft = false
title = "About"

+++

This website was created by **Josa Wode**, who is also responsible for this sites content. For contents of other websites linked here only their maintainers are responsible.
For further information please contact me via <a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">email</a>.  

Diese Webseite wurde erstellt von <b>Josa Wode</b>, der auch für den Inhalt dieser Seite verantwortlich ist. Für die Inhalte anderer hier verlinkter Webseiten sind einzig die jeweiligen Betreiber verantwortlich.
Weitere Informationen können bei Bedarf per <a href="&#x6d;&#x61;&#x69;&#x6c;&#x74;&#111;&#x3a;&#99;&#x6f;&#100;&#x69;&#x6e;&#103;&#x40;&#102;&#111;&#116;&#111;&#101;&#x6c;&#101;&#x63;&#116;&#x72;&#105;&#99;&#x73;&#x2e;&#x64;&#x65;">Email</a> erfragt werden.
